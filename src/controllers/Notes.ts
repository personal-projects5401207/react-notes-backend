import NotesModel from "../models/Notes";

export async function createNote(data: any) {
  try {
    return await NotesModel.create(data);
  } catch (error: any) {
    throw new Error(error.message);
  }
}
