import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import AdminUser from "../models/AdminUsers";

const secretkey = process.env.API_SECRET || "FAVHICKER12345678";

export const verifyToken = async (req: any, res: any, next: NextFunction) => {
  console.log("verifyToken", req.path);
  try {
    if (req.path === "/api/notes") {
      return next();
    }

    const authorizationHeader = req.headers.authorization;

    if (!authorizationHeader || !authorizationHeader.startsWith("JWT ")) {
      return res.status(401).json({ message: "Invalid Token" });
    }

    const token = authorizationHeader.split(" ")[1];
    const decoded = jwt.verify(token, secretkey) as jwt.JwtPayload;

    if (!decoded) {
      return res.status(401).json({ message: "Invalid Token" });
    }

    const user = await AdminUser.findOne({ _id: decoded.id });

    if (!user || user.del === 1) {
      return res.status(401).json({ message: "Invalid Token" });
    }

    await AdminUser.findByIdAndUpdate(user._id, { last_seen: new Date() });

    req["user"] = user;
    req["token"] = token;
    next();
  } catch (error) {
    console.error("Token verification error:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

export default verifyToken;
