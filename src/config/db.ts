import mongoose, { connect } from "mongoose";

const mongoString: string = process.env.DATABASE_URL || "";

function connectDB() {
  return connect(mongoString)
    .then(() => {})
    .catch((err) => {});
}

export default connectDB;
