import express, { Request, Response } from "express";
import NotesModel from "../models/Notes";
import { createNote } from "../controllers/Notes";
import mongoose from "mongoose";

const router = express.Router();

router.get("/notes", async (req: any, res: any) => {
  try {
    const page: number = parseInt(req.query.page as string) || 1;
    const limit: number = parseInt(req.query.limit as string) || 10;
    // const totalCount: number = await NotesModel.countDocuments();

    let filter: any = req.query.filter;
    let where_filter: any = {};
    if (filter) {
      for (const property in filter) {
        if (filter[property] == "-") {
          where_filter[property] = "";
        } else {
          where_filter[property] = { $regex: filter[property], $options: "i" };
        }
      }
    }

    let data = await NotesModel.find(where_filter)
      .skip((page - 1) * limit)
      .limit(limit);

    res.json({ data });
  } catch (error) {
    console.error("Error getting notes:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.post("/notes", async (req, res) => {
  console.log("request", req.body);
  try {
    const result = await createNote(req.body);

    res.status(201).json({
      success: true,
      data: result,
      message: "Note created successfully",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      success: false,
      error: "Internal Server Error",
    });
  }
});

router.get("/notes/:id", async (req: Request, res: Response) => {
  try {
    const data = await NotesModel.findById(req.params.id);
    if (!data) {
      res.status(404).json({ message: "Note not found" });
    } else {
      res.json(data);
    }
  } catch (error) {
    res.status(400).json({ message: (error as Error).message });
  }
});

router.put("/notes/:id", async (req: Request, res: Response) => {
  console.log("requestUpdate", req.body);
  try {
    const data = await NotesModel.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (!data) {
      res.status(404).json({ message: "Note not found" });
    } else {
      res.json(data);
    }
  } catch (error) {
    res.status(400).json({ message: (error as Error).message });
  }
});

router.delete("/notes/:id", async (req: Request, res: Response) => {
  console.log("requestDelete", req.body);
  try {
    const data = await NotesModel.findByIdAndDelete(req.params.id);
    if (!data) {
      res.status(404).json({ message: "Note not found" });
    } else {
      res.json({ message: "Note deleted" });
    }
  } catch (error) {
    res.status(400).json({ message: (error as Error).message });
  }
});

module.exports = router;
