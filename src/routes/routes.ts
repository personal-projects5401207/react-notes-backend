import express from "express";

const Notes = require("./Notes");

const router = express.Router();

router.use("/notes", Notes);

const devRoutes = [
  {
    path: "",
    route: Notes,
  },
];

devRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

export { router };
