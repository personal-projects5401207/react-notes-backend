import mongoose, { Schema, model, Document } from "mongoose";

interface Note {
  title: string;
  content: string;
}

const noteSchema = new Schema<Note>({
  title: { type: String, required: true },
  content: { type: String, required: true },
});

const NotesModel = mongoose.model<Note>("notes", noteSchema);

export default NotesModel;
