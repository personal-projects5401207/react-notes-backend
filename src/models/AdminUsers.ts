import mongoose,{ Schema, model, Document } from "mongoose";
import { isEmail } from 'validator'; 

interface AdminUser {
  admin_user_id: number;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  created?: Date;
  last_seen?:Date;
  del?: number;
  status_id?: number;
  status?: string;
}

const adminUserSchema = new Schema<AdminUser>({
  admin_user_id: {
    required: true,
    type: Number,
  },
  first_name: {
    type: String,
    required: [true, "first_name not provided"],
  },
  last_name: {
    type: String,
    required: [true, "last_name not provided"],
  },
  email: {
    type: String,
    unique: true, // set to boolean
    lowercase: true,
    trim: true,
    required: [true, "email not provided"],
    validate: [isEmail, "{VALUE} is not a valid email!"],
  },
  password: {
    type: String,
    required: true,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  last_seen: {
    type: Date,
    default: Date.now,
  },
  del: {
    type: Number,
  },
  status_id: {
    type: Number,
  },
  status: {
    type: String,
  },
});

const AdminUser = mongoose.model<AdminUser>("admin_users", adminUserSchema);
export default AdminUser;
